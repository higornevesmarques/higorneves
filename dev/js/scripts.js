$('document').ready(function() {
    $('.nav__link').click(function(event) {
        event.preventDefault();
        var hash = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 300);
    });

    $('.hamburger').click(function() {
        $('#nav').slideToggle(300);

        $('.nav__link').click(function(event) {
            event.preventDefault();
            $('.hamburger').removeClass('is-active');
            $('#nav').slideUp(300);
        })
    });
});
