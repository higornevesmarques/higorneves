"use strict";

module.exports = function(grunt) {

  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    watch: {
      html: {
        files: "dev/html/**/*.html",
        tasks: ["includes"]
      },
      images: {
        files: "dev/img/**/*.{png,jpg,gif,jpeg}",
        tasks: ["imagemin"]
      },
      sass: {
        files: "dev/scss/**/*.scss",
        tasks: ["sass", "cmq", "postcss", "cssmin"]
      },
      js: {
        files: "dev/js/*.js",
        tasks: ["uglify", "concat"]
      },
      svg: {
        files: "dev/svg/**/*.svg",
        tasks: ["svgmin"]
      }
    },

    includes: {
      files: {
        src: '**/*.html',
        dest: '.',
        flatten: true,
        cwd: 'dev/html',
        options: {
          silent: true,
          banner: ''
        }
      }
    },

    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'production/css/style.css': 'dev/scss/style.scss'
        }
      }
    },

    postcss: {
      options: {
        processors: [
          require('autoprefixer')({
            browsers: 'last 3 versions'
          })
        ]
      },
      dist: {
        src: [
          'production/css/style.css'
        ]
      }
    },

    cmq: {
      options: {
        log: true
      },
      dist: {
        files: {
          'production/css/style.css': 'production/css/style.css'
        }
      }
    },

    cssmin: {
      options: {
        keepSpecialComments: 0,
        noAdvanced: true,
        banner: ''
      },
      css: {
        files: {
          "arquivos/higorneves.css": "production/css/style.css"
        }
      }
    },

    svgmin: {
      options: {
        plugins: [{
          removeViewBox: false
        }, {
          removeUselessStrokeAndFill: false
        }, {
          removeEmptyAttrs: false
        }]
      },
      dist: {
        expand: true,
        cwd: 'dev/svg',
        src: '**/*.svg',
        dest: 'production/svg'
      }
    },

    uglify: {
      options: {
        mangle: false
      },
      dist: {
        files: {
          "production/js/scripts.min.js": "dev/js/scripts.js",
          "production/js/arbitrary-anchor.min.js": "dev/js/arbitrary-anchor.js",
          // "production/js/mobile-detect.min.js": "dev/js/plugins/mobile-detect.js",
          // "production/js/slick.min.js": "dev/js/plugins/slick.js",
          // "production/js/formzin.min.js": "dev/js/plugins/formzin.js",
          // "production/js/lazyload.min.js": "dev/js/plugins/lazyload.js",
          // "production/js/fitvids.min.js": "dev/js/plugins/fitvids.js",
          // "production/js/easyzoom.min.js": "dev/js/plugins/easyzoom.js",
          // "production/js/handlebars.min.js": "dev/js/plugins/handlebars.js",
          // "production/js/touch-events.min.js": "dev/js/plugins/touch-events.js",
          // "arquivos/account.js": "dev/js/account.js",
          // "arquivos/product.js": "dev/js/product.js",
          // "arquivos/lojas.js": "dev/js/lojas.js",
          // "arquivos/lookbook.js": "dev/js/lookbook.js",
          // "arquivos/infinity-scroll.js": "dev/js/plugins/infinity-scroll.js",
        }
      }
    },

    concat: {
      options: {
        separator: ';'
      },
      base: {
        src: [
          'production/js/scripts.min.js',
          'production/js/arbitrary-anchor.min.js'
          // 'production/js/mobile-detect.min.js',
          // 'production/js/fitvids.min.js',
          // 'production/js/easyzoom.min.js',
          // 'production/js/lazyload.min.js',
          // 'production/js/slick.min.js',
          // 'production/js/formzin.min.js',
          // 'production/js/handlebars.min.js',
          // 'production/js/touch-events.min.js'
        ],
        dest: 'arquivos/higorneves.js'
      }
    },

    imagemin: {
      dynamic: {
        options: {
          optimizationLevel: 7,
          progressive: true
        },
        files: [{
          expand: true,
          cwd: 'dev/img/',
          src: ['**/*.{png,jpg,gif,jpeg}'],
          dest: 'arquivos/'
        }]
      }
    },

    browserSync: {
      files: {
        src: [
          '*.html',
          'arquivos/*.css',
          'arquivos/*.js'
        ]
      },
      options: {
        watchTask: true,
        server: {
          baseDir: './'
        },
        ghostMode: {
          clicks: false,
          scroll: false,
          links: true,
          forms: true
        }
      }
    },

    devUpdate: {
      main: {
        options: {
          updateType: 'force',
          reportUpdated: false,
          semver: false,
          packages: {
            devDependencies: true,
            dependencies: false
          },
          packageJson: null,
          reportOnlyPkgs: []
        }
      }
    }

  });

  grunt.registerTask("default", ["includes", "browserSync", "watch"]);
  grunt.registerTask("html", ["includes"]);
  grunt.registerTask("css", ["sass", "cmq", "postcss", "cssmin"]);
  grunt.registerTask("img", ["imagemin"]);
  grunt.registerTask("js", ["uglify", "concat"]);
  grunt.registerTask("svg", ["svgmin"]);
  grunt.registerTask("compile", ["sass", "cmq", "postcss", "cssmin", "imagemin", "uglify", "concat", "svgmin"]);
  grunt.registerTask("update", ["devUpdate"]);

};
